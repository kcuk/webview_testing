import { Component } from '@angular/core';
// import { Array } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(){
    document.addEventListener("DOMContentLoaded", function(event) { 
      document.getElementById("activeButton").click();
   });
  }
  title = 'testing';

  isValid1:boolean=false;
isValid2:boolean=true;

  obj1='4%';
  obj2='45%';
  obj3='86%';
  obj4='null';
  obj5='null';
  obj6='null';

  disp1:string='block';
  disp2:string='block';
  disp3:string='block';
  disp4:string='none';
  disp5:string='none';
  disp6:string='none';


  count:number = 0;




  
b1Click(){

  console.log("b1Clicked ------------");

  if(this.count == 0){
    this.isValid1=false;
    console.log("b1clicked disabled");
    this.isValid2=true;
  }
  else if(this.count > 4){
    this.count=this.count%4;
  }

  else if(this.count<=4){
    this.count=this.count;
    console.log(this.count,"this.count inside the this.count<=4");

    if(this.count == 1){
      this.obj1='4%';
    this.obj2='45%';
    this.obj3 = '86%';
    
    this.disp1='block';
    this.disp2='block';
    this.disp3='block';

    this.disp4='none';
    this.disp5='none';
    this.disp6='none';
  
    this.count=this.count-1;
    this.isValid2=true;
    this.isValid1=false;

    document.getElementById("view2").scrollIntoView({block: 'start', behavior: 'smooth'});
    window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });

    }
  
    else if(this.count == 2){
      this.obj2='4%';
      this.obj3='45%';
      this.obj4='86%';
  
      this.disp1='none';
      this.disp5='none';
      this.disp6='none';
  
      this.disp2='block';
      this.disp3='block';
      this.disp4='block';
    
      this.count=this.count-1;
      
      this.isValid2=true;

      document.getElementById("view3").scrollIntoView({block: 'start', behavior: 'smooth'});

    }
  
    else if(this.count == 3){
      this.obj3='4%';
      this.obj4='45%';
      this.obj5='85%';
  
      this.disp1='none';
      this.disp2='none';
      this.disp6='none';
      this.disp3='block';
      this.disp4='block';
      this.disp5='block';
  
      this.count=this.count-1;

      this.isValid2=true;

      document.getElementById("view4").scrollIntoView({block: 'start', behavior: 'smooth'});

    }
  
    else if(this.count == 4){
      console.log("this.count == 4");
      this.obj4='4%';
      this.obj5='45%';
      this.obj6='85%';
  
      this.disp3='none';
      this.disp1='none';
      this.disp2='none';
      this.disp4='block';
      this.disp5='block';
      this.disp6='block';
  
      this.count=this.count-1;

      this.isValid2=true;

      document.getElementById("view5").scrollIntoView({block: 'start', behavior: 'smooth'});

    }
  
    else{
      console.log("button not allowed to click");
    }
  }
  
}


b2Click(){
  console.log("=================== b2Clicked ------------");
  this.count=this.count+1;


  if(this.count == 1){
    this.disp1='none';
    this.disp5='none';
    this.disp6='none';
    this.disp4='block';

    this.obj2='4%';
    this.obj3='45%';
    this.obj4='86%';

    this.isValid1=true;
  }
  else if(this.count == 2){
    console.log("----------count 2 clicks---------------");
    this.disp1='none';
    this.disp2='none';
    this.disp6='none'; 

    this.disp5='block';

    this.obj3='4%';
    this.obj4='45%';
    this.obj5='86%';


  }
  else if (this.count ==3){

    this.disp1='none';
    this.disp2='none';
    this.disp3='none';
    
    this.disp6='block';
    
    this.obj4='4%';
    this.obj5='45%';
    this.obj6='86%';
  }

  else if(this.count == 4   ){

    this.disp1='none';
    this.disp2='none';
    this.disp3='none';
    this.disp4='none';

    this.obj5='4%';
    this.obj6='45%';

    this.isValid2=false;
  }
}

// ===========
 // display div of testimonials
 t1:string='block';
 t2:string='none';
 t3:string='none';


 //for *ngIf
 tr1:boolean=true;
 tr2:boolean=false;
 tr3:boolean=false;

 testCount:number = 0;

 forwardBtn:boolean = true;
 backwardBtn:boolean = false;

 testBackwardClick(){

this.testCount =this.testCount-1;
console.log(this.testCount,"backward button clicked");

if(this.testCount > 2){
     this.testCount = this.testCount % 2;
   }

   else if(this.testCount < 2){
            if(this.testCount == 0){
     //  this.msg.add({severity:'success', summary: 'Success Message', detail:'Order submitted'});
     this.tr1=true;
     this.tr2=false;
     this.tr3=false;

     this.t1='block';
     this.t2='none';
     this.t3='none';

   this.testCount =0;
   this.backwardBtn=false;
   this.forwardBtn=true;

   document.getElementById('t1').className ='ani-active';
    }
  else if(this.testCount == 1 ){
              this.tr1=false;
              this.tr2=true;
              this.tr3=false;

              this.t1='none';
              this.t2='block';
              this.t3='none';

              this.forwardBtn=true;


   document.getElementById('t2').className ='ani-active';
            }
   }
 }



 testForwardClick(){
  //  this.msg.add({severity:'success', summary: 'Success Message', detail:'Order submitted'});
  console.log("forward button clicked");
   this.testCount=this.testCount+1;

   console.log(this.testCount,"================= forwardCount ===============");
   if(this.testCount == 1){
     this.tr1=false;
     this.tr2=true;
     this.tr3=false;

     this.t1='none';
     this.t2='block';
     this.t3='none';

     this.backwardBtn=true;

     document.getElementById('t2').className ='ani';

   }
   else if(this.testCount == 2){
     this.tr1 =false;
     this.tr2=false;
     this.tr3=true;

     this.t1='none';
     this.t2='none';
     this.t3='block';

     this.forwardBtn=false;
     this.backwardBtn=true;

     document.getElementById('t3').className ='ani';

   }
   
 }

}
